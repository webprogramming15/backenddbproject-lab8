import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true }) // ไม่ให้ซ้ำ
  name: string;

  @Column()
  price: number;
}
